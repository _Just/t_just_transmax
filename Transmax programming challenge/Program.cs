﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Transmax_programming_challenge
{
	public class Program
	{
		static void Main(string[] args)
		{
			//Get fileName
			string readInput;
			if (args.Length >= 1)
			{
				readInput = args[0];
			}
			else
			{
				Console.Write("File path: ");
				readInput = Console.ReadLine();
			}
			if (readInput.LastIndexOf(".txt") != readInput.Length - 4)
			{
				readInput += ".txt";
			}

			//Read file
			string[] fileLines;
			try
			{
				fileLines = File.ReadAllLines(readInput);
			}
			catch
			{
				Console.WriteLine("File could not be found.");
				return;
			}

			//Sort
			string[] sorted;
			try
			{
				sorted = grade_scores(fileLines);
			}
			catch
			{
				return;
			}

			//Create new file
			string newpath = pathBuilder(readInput);
			
			if (File.Exists(newpath)) {
				File.Delete(newpath);
			}
			File.WriteAllLines(newpath, sorted);

			//output
			Console.WriteLine("File has been sorted:");
			foreach (var i in sorted)
			{
				Console.WriteLine(i);
			}
			Console.WriteLine("Exported to " + newpath);
		}

		public static string pathBuilder(string original)
		{
			//Create new filepath
			return original.Insert(original.Length - 4, "-graded");
		}

		public static string[] grade_scores(string[] input)
		{
			int n = input.Length;

			//Contact list
			List<Contact> contacts = new List<Contact>();
			for (var i = 0; i < n; i++)
			{
				try
				{
					contacts.Add(new Contact(input[i]));
				}
				catch (Exception e)
				{
					Console.WriteLine(e.Message);
				}
			}

			//Sort
			string[] sorted;
			try
			{
				contacts = contacts.OrderByDescending(o => o.score).ThenBy(o => o.lastName).ThenBy(o => o.firstName).ToList();
				sorted = contacts.ConvertAll(o => o.ToString()).ToArray();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				return new string[0];
			}
			
			return sorted;
		}
	}

	public class Contact
	{
		public string firstName;
		public string lastName;
		public int score;

		public Contact(string input)
		{
			string[] split = input.Split(',');
			if (split.Length != 3 || !Int32.TryParse(split[2], out this.score))
			{
				throw new Exception("Contact is not in correct format.");
			}
			this.firstName = split[1];
			this.lastName = split[0];
		}

		public override string ToString()
		{
			return this.lastName + "," + this.firstName + ", " + this.score;
		}
	}
}