﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Transmax_programming_challenge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transmax_programming_challenge.Tests
{
	[TestClass()]
	public class ContactTests
	{
		[TestMethod()]
		public void Contact_Test()
		{
			Contact[] test = new Contact[3];
			try
			{
				test[0] = new Contact("");
			}
			catch (Exception e)
			{
				Assert.AreEqual("Contact is not in correct format.", e.Message);
			}
			try
			{
				test[1] = new Contact("A1, A2, A3");
			}
			catch (Exception e)
			{
				Assert.AreEqual("Contact is not in correct format.", e.Message);
			}
			test[2] = new Contact("lName, fName, 100");

			Assert.IsNull(test[0]);
			Assert.IsNull(test[1]);
			Assert.AreEqual(test[2].score, 100);
			Assert.IsInstanceOfType(test[2].score, 0.GetType());
			Assert.AreEqual(test[2].firstName, " fName");
			Assert.AreEqual(test[2].lastName, "lName");
		}

		[TestMethod()]
		public void Contact_ToString_Test()
		{
			Contact[] input = new Contact[]
			{
				new Contact("fName, lName, 0"),
				new Contact(", , 0"),
				new Contact("000, 000, 000"),
				new Contact("fName, lName, 123456789"),
				new Contact("fName mName, lName, 0")
			};
			string[] output = new string[]
			{
				"fName, lName, 0",
				", , 0",
				"000, 000, 0",
				"fName, lName, 123456789",
				"fName mName, lName, 0"
			};

			for (var i = 0; i < input.Length; i++)
			{
				Assert.AreEqual(input[i].ToString(), output[i]);
			}
		}
	}
}