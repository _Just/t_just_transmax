﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Transmax_programming_challenge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transmax_programming_challenge.Tests
{
	[TestClass()]
	public class ProgramTests
	{
		[TestMethod()]
		public void pathBuilderTest()
		{
			//takes string and adds "-graded" before ".txt"
			string[] input = new string[]
			{
				"abc.txt",
				".txt",
				"_000.txt"
			};
			string[] output = new string[]
			{
				"abc-graded.txt",
				"-graded.txt",
				"_000-graded.txt"
			};

			for (var i = 0; i < input.Length; i++)
			{
				if (Program.pathBuilder(input[i]) != output[i])
				{
					Assert.Fail();
				}
			}
		}

		[TestMethod()]
		public void sortTest1()
		{
			//Correctly formatted input
			string[] input = new string[]
			{
				"A1, A2, 0",
				"B1, B2, 75",
				"C1, C2, 50",
				"D1, D2, 100",
				"E1, E2, -100",
				"F1, F2, 25"
			};
			string[] output = new string[]
			{
				"D1, D2, 100",
				"B1, B2, 75",
				"C1, C2, 50",
				"F1, F2, 25",
				"A1, A2, 0",
				"E1, E2, -100"
			};

			string[] test = Program.grade_scores(input);
			for (var i = 0; i < test.Length; i++)
			{
				Assert.AreEqual(test[i], output[i]);
			}
		}

		[TestMethod()]
		public void sortTest2()
		{
			//Incorrectly formatted input
			string[] input = new string[]
			{
				"A1, A2",
				"B1, B2, B3, 75",
				"C1, C2, 0",
				"D1, D2, D3",
				"E1, E2, 100",
				"50, 50, 50",
				"aaaaaaaa"
			};
			string[] output = new string[]
			{
				"E1, E2, 100",
				"50, 50, 50",
				"C1, C2, 0"
			};

			string[] test = Program.grade_scores(input);
			for (var i = 0; i < test.Length; i++)
			{
				Assert.AreEqual(test[i], output[i]);
			}
		}

		[TestMethod()]
		public void sortTest3()
		{
			//Empty input
			string[] test = Program.grade_scores(new string[0]);
			if (test.Length != 0)
			{
				Assert.Fail();
			}
		}
	}
}